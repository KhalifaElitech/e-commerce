<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert([
            'name' => "Homme",
        ]);
        DB::table('categories')->insert([
            'name' => "Femme",
        ]);
        DB::table('categories')->insert([
            'name' => "Enfant",
        ]);
    }
}
