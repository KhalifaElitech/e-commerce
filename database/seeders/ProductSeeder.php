<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i = 1; $i <= 10; $i++){
            if ($i < 4) {
                DB::table('products')->insert([
                    'name' => "Product_$i",
                    'price' => 1000 * $i,
                    'category_id' => 1,
                    'description' => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga obcaecati culpa et inventore fugiat odit illum optio praesentium voluptatem sapiente unde accusantium doloremque consectetur, nostrum quidem minima explicabo aut vero."
                ]);
            }elseif ($i >= 4 && $i < 8) {
                DB::table('products')->insert([
                    'name' => "Product_$i",
                    'price' => 1000 * $i,
                    'category_id' => 2,
                    'description' => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga obcaecati culpa et inventore fugiat odit illum optio praesentium voluptatem sapiente unde accusantium doloremque consectetur, nostrum quidem minima explicabo aut vero."
                ]);
            }else {
                DB::table('products')->insert([
                    'name' => "Product_$i",
                    'price' => 1000 * $i,
                    'category_id' => 3,
                    'description' => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga obcaecati culpa et inventore fugiat odit illum optio praesentium voluptatem sapiente unde accusantium doloremque consectetur, nostrum quidem minima explicabo aut vero."
                ]);
            }
        }
    }
}
