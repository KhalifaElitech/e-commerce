@extends('layouts.layout')

@section('title')
    {{ $user->name }}
@endsection

@section('body')
    <div class="container m-5">
        <div class="row">
            <div class="col-12">
                <div class="form">
                    <strong>Nom complet : </strong>
                    {{ $user->name }}
                </div>
            </div>
            <div class="col-12">
                <div class="form">
                    <strong>Email : </strong>
                    {{ $user->email }}
                </div>
            </div>
            <div class="col-12">
                <div class="form">
                    <strong>Rôles :</strong>
                    @if (!empty($user->getRoleNames()))
                        @foreach ($user->getRoleNames() as $role)
                            <label for="" class="badge badge-success">
                                {{ $role }}
                            </label>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection('body')
