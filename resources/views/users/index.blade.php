@extends('layouts.layout')

@section('title')
    Utilisateurs
@endsection

@section('body')

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="pull-left">
                    <h2>Gestion des utilisateurs</h2>
                </div>
                <div class="pull-right">
                    <a href="{{ route('users.create') }}" class="btn btn-outline-warning">Enregistrer un nouvel utilisateur</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">

                @if ($message = Session::get('success'))
                    <p>{{ $message }}</p>
                @endif

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th width="280px">Nom complet</th>
                            <th>Email</th>
                            <th>Rôles</th>
                            <th colspan="3" width="px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @if (intval(count($user->getRoleNames())) != 0)
                                        @foreach ($user->getRoleNames() as $role)
                                            <label for="" class="badge badge-success">
                                                {{ $role }}
                                            </label>
                                        @endforeach
                                    @else
                                        <label for="" class="badge badge-secondary">
                                            Pas de rôle
                                        </label>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-info" href="{{ route('users.show', $user->id) }}">
                                        <span class="fa fa-eye"></span>
                                    </a>
                                </td>
                                <td>
                                    <a class="btn btn-info" href="{{ route('users.edit', $user->id) }}">
                                        <span class="fa fa-edit"></span>
                                    </a>
                                </td>
                                <td>
                                    <a class="btn btn-info" href="#">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        {!! $users->render() !!}
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

@endsection
