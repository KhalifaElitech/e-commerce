@extends('layouts.layout')

@section('title')
    Enregistrer un nouvel utilisateur
@endsection

@section('body')

    <div class="container m-5">
        <div class="row">
            <div class="col-12">
                {!! Form::open(array('route' => 'users.store', 'method' => 'POST', 'class' => 'contact-form row', 'id' => 'main-contact-form')) !!}
                    <div class="col-12">
                        <div class="form-group">
                            <strong>Nom Complet</strong>
                            {!! Form::text('name', null, ['placeholder' => 'Nom utilisateur', 'class' => 'form-control mb-2']) !!}
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <strong>Email</strong>
                            {!! Form::email('email', null, ['placeholder' => 'Email', 'class' => 'form-control mb-2']) !!}
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <strong>Mot de passe</strong>
                            {!! Form::password('password', null, ['placeholder' => 'Mot de passe', 'class' => 'form-control mb-2']) !!}
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <strong>Confirmer de passe</strong>
                            {!! Form::password('confirm-password', null, ['placeholder' => 'Confirmer votre mot de passe', 'class' => 'form-control mb-2']) !!}
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <strong>Rôles</strong>
                            {!! Form::select('roles[]', $roles, [], ['class' => 'form-control', 'multiple']) !!}
                        </div>
                    </div>
                    <div class="col-12">
                        {!! Form::submit('Enregistrer', ['class' => 'btn btn-block btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
