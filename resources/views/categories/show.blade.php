@extends('layouts.app')

@section('content')
    <table>
        <tbody>
            <tr>
                <td>ID : {{ $category->id }}</td>
            </tr>
            <tr>
                <td>Nom : {{ $category->name }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>
                    <a href="{{ URL::previous() }}">Retour à la liste des catégories</a>
                </td>
            </tr>
        </tfoot>
    </table>
@endsection