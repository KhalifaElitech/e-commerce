<fieldset>
    <label for="name">Nom de la catégorie</label>
    <input type="text" name="name" value="{{ $category->name ?? "" }}" id="name">
    <br>
    <input type="submit" value="Enregistrer">
    <br>
    <a href="{{ route("categories.index") }}">Page Accueil</a>
</fieldset>