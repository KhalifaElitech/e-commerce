@extends('layouts.app')

@section('content')
    
    <h3>Le formulaire d'ajout d'une nouvelle catégorie</h3>

    <form action="{{ route('categories.store') }}" method="POST">
        @csrf
        @include('categories.form')
    </form>

@endsection