<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>La liste de toutes mes catégories</h1>

    @if ($message = Session::get('success'))
        <p>{{ $message }}</p>
    @endif

    <table border="1">
        <thead>
            <th>Nom</th>
            <th colspan="3">Actions</th>
        </thead>
        <tbody>
            @foreach ($categories as $category)
                <tr>
                    <td>{{ $category->name }}</td>
                    <td>
                        <a href="{{ route('categories.show', $category->id) }}">
                            Afficher
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('categories.edit', $category->id) }}">
                            Modifier
                        </a>
                    </td>
                    <td>
                       <form action="{{ route('categories.destroy', $category->id) }}" method="POST">
                           @csrf
                           @method('DELETE')
                           <input type="submit" value="Supprimer">
                       </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
        
    </table>

    <br>

    <a href="{{ route("categories.create") }}">Ajouter une nouvelle catégorie</a>
</body>
</html>