@extends('layouts.app')

@section('content')
    
    <h3>Modification de la catégorie "{{ $category->name }}"</h3>

    <form action="{{ route('categories.update', $category->id) }}" method="POST">
        @csrf
        @method('PUT')
        @include('categories.form')
    </form>
@endsection