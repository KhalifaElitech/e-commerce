<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    //
    public function home()
    {
        $categories = Category::orderby('id', 'desc')->get() ;

        // dd($categories[0]->products()->get()) ;

        return view('home', compact('categories')) ;
    }

    public function logout()
    {

        Auth::logout();

        return redirect()->route('login') ;
    }
}
