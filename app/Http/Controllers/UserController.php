<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //

    public function index(Request $request)
    {
        $users = User::orderBy('id', 'desc')->paginate() ;

        return view('users.index', compact('users'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();

        return view('users.create', compact('roles')) ;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all() ;
        $input['password'] = Hash::make($input['password']) ;

        $user = User::create($input) ;
        $user->assignRole($request->input('roles')) ;

        return redirect()->route('users.index')->with('success', 'Utilisateur enregistré avec succès') ;
    }

    public function show($id)
    {
        $user = User::find($id) ;

        return view('users.show', compact('user')) ;
    }
}
