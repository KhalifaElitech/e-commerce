<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    //Récupération de tous les produits d'une catégorie
    public function products()
    {
        return $this->hasMany(Product::class) ;
    }
}
